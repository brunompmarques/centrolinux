---
title: "O Centro Linux"
description: "Sobre o Centro Linux?"
featured_image: ''
menu:
  main:
    weight: 1
---


O Centro Linux é uma iniciativa que procura proporcionar experiências comunitárias práticas em um local físico. Workshops, hackatons, demonstrações, instalações, apoio técnico comunitário, etc... E como tal não só tem um laboratório onde os participantes podem de facto por "as mãos no teclado", como o tem em um dos melhores locais possíveis para uma comunidade deste tipo, um maker space.


# FAQ
1. [O que é o Centro Linux?](https://centrolinux.pt/oquee)
2. [Como funciona o Centro Linux?](https://centrolinux.pt/funcionamento)
3. [Como contribuir para o Centro Linux?](https://centrolinux.pt/contruir)
4. [Onde está o Centro Linux?](https://centrolinux.pt/ondeEstamos)
5. [Como é a privacidade no Centro Linux?](https://centrolinux.pt/privacidade)
6. [Como é que posso contactar o Centro Linux?](https://centrolinux.pt/contact)


#### Apoios:

[![Ubuntu-pt](https://centrolinux.pt/images/BLHCBzYG_400x400.jpg "Ubuntu-pt")](https://ubuntu-pt.org)
[![Localização do MILL](https://centrolinux.pt/images/web_header.png "Makers In Litle Lisbon")](https://mill.pt)
