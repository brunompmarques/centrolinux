---
title: Funcionamento
featured_image: ''
description: Como funciona o Centro Linux
omit_header_text: false
type: page
menu: main
---

# Missão:

A missão do Centro Linux é a de promover a partilha de conhecimento sobre GNU/Linux, Software Livre e Cultura Livre, com base na experimentação prática comunitária.

É feito pela comunidade e para a comunidade.

# Valores do Centro Linux:

* Respeito para com os outros
* Oportunidade
* Partilha de conhecimento
* Experimentação
* Construir e contruído pela comunidade
* Fazer amizades

# Quando funciona?

O Centro Linux funciona quando tem actividades planeadas.

Apesar da generosidade do [Makers In Little Lisbon - MILL](https://mill.pt) em acolher o Centro Linux, o Centro não tem instalações permanentes exclusivamente dedicadas, nem pessoal de forma permanente, mas sim voluntários.

O acordo actual com o MILL dedica o último Sábado de cada mês a actividades do Centro Linux, mas isto é em cada caso susceptível a confirmação e até mesmo a alteração conforme seja necessário. Apesár disso queremos que a data da realização das actividades do Centro Linux seja previsível.

# Quais as regras, e comportamentos aceitáveis?

O Centro Linux é uma iniciativa promovida pela [Comunidade Ubuntu Portugal](https://ubuntu-pt.org) e por isso segue o [Código de Conduta do Ubuntu (em língua inglêsa)](https://ubuntu.com/community/governance/code-of-conduct) e a sua [Política de Diversidade](https://ubuntu.com/community/governance/diversity).

Podem aprender mais sobre a [Comunidade Ubuntu no site oficial](https://ubuntu.com/community).

O Centro Linux está alojado no MILL, por isso as regras e comportamentos aceitáveis de acordo com o MILL, são também aplicáveis ao Centro Linux.