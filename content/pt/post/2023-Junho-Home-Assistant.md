---
#date: 2023-04-29
description: "Explorar colectivamente o Home Assistant"
featured_image: ""
tags: ["2023", "Junho", "Home Assistant", "Domótica", "Automação", "Casa Inteligente", "Linux"]
title: "Exploração do Home Assistant"
---

[![Cartaz](https://centrolinux.pt/images/2023.06-Explorar-Home-Assistant/post.base.png )](https://osm.org/go/b5crq_xVM?layers=N)


# Exploração colectiva do Home Assistant

De acordo com a [Wikipédia o Home Assistant](https://en.wikipedia.org/wiki/Home_Assistant) é um sistema de controlo central para dispositivos de casa inteligente/domótica, com um foco no controlo local (permitindo evitar cloud ou serviços de terceiros) e em privacidade.

Neste evento vamos como comunidade explorar o Home assistant para aprendermos a instalar e utilizar para tornarmos as nossas casas mais cómodas, mais acessíveis, mais divertidas e até explorar oportunidades baixar alguns custos como os de utilização da electricidade. Para isto alguns membros mais experientes da comunidade vêm partilhar connosco o seu conhecimento e experiências e vamos todos ter oportunidade de ter contacto directo com o Home Assistant e fazer algumas experiências para sairmos do Centro Linux com algum conhecimento elementar e muita inspiração sobre o que fazermos quando chegarmos a casa.

**Estás interessado em aprender sobre *home automation*? Então vem explorar com a comunidade!**

**Queres partilhar os teus conhecimentos e experiências com *home automation* e Home Assistant? Então vem partilhar com a comunidade!**

## Quando?

No **Sábado dia 10 de Junho de 2023 a partir das 11:30**
Haverá interrupção das actividades para almoço.

### Plano (provisório) de actividades:

| Hora  | Descrição                                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 10:00 | Pequeno-almoço linuxeiro *([detalhes aqui](https://centrolinux.pt/pequeno-almoco-linuxeiro/)), (a confirmar por ser feriado nacional)* |
| 11:00 | Chegada dos voluntários ao Centro Linux                                      |
| 11:05 | Montagem do espaço para as actividades                                       |
| 11:30 | Iníco das actividades                                                        |
| 12:00 | Gravação de episódio ao vivo de um episódio do [Podcast Ubuntu Portugal](https://www.podcastubuntuportugal.org) sobre o tema do evento |
| 13:00 | Interrupção para o almoço                                                    |
| ----- | ---------------------------------------------------------------------------- |
| 15:00 | Retomar das actividades no Centro Linux                                      |
| 18:45 | Fim das actividades e desmontagem e arrumação do espaço                      |
| 19:00 | Fim das actividades                                                          |
| ----- | ---------------------------------------------------------------------------- |


### Local:

O Workshop será realizado no Centro Linux que se localiza no Makers In Little Lisbon - MILL, que é em Lisboa, muito perto do Campo dos Mártires da Pátria.
Pode consultar [onde estamos](https://centrolinux.pt/ondeestamos/) para morada e informações detalhadas sobre como pode chegar ao MILL de muitas formas diferentes.

### Inscrições

As inscrições são obrigatórias e feitas [no site do MILL](https://mill.pt/agenda/13870/).

No caso deste evento em especifico há dois tipos de bilhetes:
1. bilhete para a manhã/gravação do episódio do Podcast Ubuntu Portugal
2. bilhete para as actovidades do dia inteiro


### Código de Conduta

O Centro Linux é organizado e promovido pela Comunidade Ubuntu-pt (sem que esteja limitado a participações e contribuições da mesma), que se rege pelo [Código de Conduta](https://ubuntu.com/community/code-of-conduct) e pela [declaração de diversidade do Ubuntu](https://ubuntu.com/community/diversity), adicionalmente por estar alojado no [Makers in Little Lisbon](https://mill.pt/), pelo qual estamos muito gratos e que por isso queremos garantir a melhor condição e ambiente possível nesse espaço.

