---
title: "Agenda"
date: 2022-10-11T12:22:11-00:00
---

# Próxima actividade

A próxima actividade do Centro Linux é:

[Exploração do Home Asistant](https://centrolinux.pt/post/2023-junho-home-assistant/).

[![Cartaz](https://centrolinux.pt/images/2023.06-Explorar-Home-Assistant/post.base.png )](https://centrolinux.pt/post/2023-junho-home-assistant/)

## Esta é a lista completa actividades do Centro Linux.

*(Inclui actividades anteriores)*
