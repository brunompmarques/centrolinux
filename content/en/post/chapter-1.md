---
date: 2022-10-11T22:15:30-00:00
description: "A actividade inaugural do Centro Linux"
tags: ["scene"]
title: "A inauguração"
---

**Inauguração do Centro Linux**

A actividade inaugural do Centro Linux vai decorrer no dia 22 de Outubro, pelas 15:30 na casa do Centro Linux o [Makers in Little Lisbon](https://mill.pt).

O evento inaugural do Centro Linux é também uma Festa de Lançamento do Ubuntu 22.10 Kinetic Kudu. 

A agenda do evento consiste de:


| Hora | Descrição |
| ---- | ------ |
| 15:30 | Boas-vindas aos convidados e participantes nas actividades, e curta apresentação do Centro Linux |
| 15:45 | Apresentação do [Makers in Little Lisbon](https://mill.pt) aos convidados |
| 16:00 | Apresentação da Comunidade Ubuntu Portugal |
| 16:15 | Pausa técnica |
| 16:30 | Início das actividades práticas:
| > | 1. Primeira montagem dos computadores e rede do laboratório do Centro Linux |
| > | 2. Instalação e experimentação com o sistema operativo Ubuntu 22.10 Kinetic Kudu nos computadores.
| > | 3. Repetição do ponto 2 por todos os que queriam experimentar o novo Ubuntu 22.10, ou simplesmente aprender a instalar Ubuntu.
| 18:30 | Palavras finais da organização |
| 19:00 | Desmontagem, arrumação e limpeza do espaço |
| 19:30 | Jantar-convivío (em lugar perto ainda da determinar) |
